const mysql = require('mysql');
const express = require('express');
const bodyparser = require('body-parser');
var app = express();
app.use(bodyparser.json());
var mysqlConnection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'Igorgej8',
    database: 'tutorial',
    multipleStatements: true
});
mysqlConnection.connect((err)=> {
    if(!err)
        console.log('Connection Established Successfully');
    else
        console.log('Connection Failed!'+ JSON.stringify(err,undefined,2));
});

const port = process.env.PORT || 8080;
app.listen(port, () => console.log(`Listening on port ${port}..`));


//ROLE
app.get('/tutorial/role', (req, res) => {
    let role =  req.body;
    mysqlConnection.query('SELECT * FROM `role`', (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});

app.get('/tutorial/role/:id', (req, res) => {
    let role = req.body;
    mysqlConnection.query('SELECT * FROM `role` WHERE `id`= ?', [req.params.id], (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});


app.post('/tutorial/role', (req, res) => {
    let role = req.body;
    const sql = 'INSERT INTO `role` (`name`) VALUES (?);';

    mysqlConnection.query(sql, [role.name], (err, rows, fields) => {
        if (!err) {
            res.send('New Role ID :' + rows.insertId);
        }
        else {
            console.log(err);
            res.send('Error');
        }
    })
});

app.put('/tutorial/role', (req, res) => {
    let role = req.body;
    const sql = 'UPDATE `role` SET `name` = (?) WHERE `id` = (?);';

    mysqlConnection.query(sql, [role.name, role.id], (err, rows, fields) => {
        if (!err)
            res.send('Role Details Updated Successfully');
        else {
            console.log(err);
            res.send('Error');

        }
    })
});


app.delete('/tutorial/role/:id', (req, res) => {
     mysqlConnection.query('DELETE FROM role WHERE id= ?', [req.params.id], (err, rows, fields) => {
         if (!err)
             res.send('Role Record deleted successfully.');
         else
             console.log(err);
     })
 });

//USER

app.get('/tutorial/user', (req, res) => {
    let user =  req.body;
    mysqlConnection.query('SELECT * FROM `user`', (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});

app.get('/tutorial/user/:id', (req, res) => {
    let user = req.body;
    mysqlConnection.query('SELECT * FROM `user` WHERE `id`= ?', [req.params.id], (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});
//setInterval(upload_random_image(), 10000);
// setInterval(() => upload_random_image(), 10000)
app.post('/tutorial/user', (req, res) => {
    let user = req.body;

    const sql = 'INSERT INTO `user` (`email` , `username` , `password`) VALUES (?, ?, ?);';
    mysqlConnection.query (sql, [user.email , user.username , user.password] , (err, rows, fields)  => {
        if (!err) {
            res.send('New User ID :' + rows.insertId);
        }
        else {
            console.log(err);
            res.send('Error');
        }
    })
});

app.put('/tutorial/user', (req, res) => {
    let user = req.body;

    const sql = 'UPDATE `user` SET `username` = (?) WHERE `id` = (?);';

    mysqlConnection.query(sql, [user.username, user.id], (err, rows, fields) => {
        if (!err)
            res.send('User Details Updated Successfully');
        else {
            console.log(err);
            res.send('Error');

        }
    })
});

app.delete('/tutorial/user/:id', (req, res) => {
    mysqlConnection.query('DELETE FROM user WHERE id= ?', [req.params.id], (err, rows, fields) => {
        if (!err)
            res.send('User Record deleted successfully.');
        else
            console.log(err);
    })
});







